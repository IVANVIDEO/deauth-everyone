# deauth-everyone

aireplay-ng script to deauth everyone from an access point 

# usage

The script takes 4 flags, all of which are required.
-a  network adapter name
-b  bssid of target AP 
-c  channel of AP
-t  how many times the script should run 
